# Flutter Practical Test



## Getting started

Hello Candidate, 

Welcome to Cupidknot Practical Exam.


Flutter App Assessment

# *Note: Use MVP or MVVM or DDD Design pattern to develop this app
API Postman Link: https://gitlab.com/piyush_ck/flutter-practical-test/-/blob/main/cupid-practical-task.postman_collection.json

This Postman contains API routes, Method(POST,GET,etc.) & Responses

Using the Above API Postman Link Build a Flutter App with following features.

- [ ] Splash Screen
- [ ] Register
- [ ] Login
- [ ] View Own Profile
- [ ] Update Profile details

**User Firebase and add below email as Editor Role.** devs@cupidknot.com

- [ ] Add Contact form with details like (Full name, Contact number, Email) and save this details on Firebase Project.
- [ ] Get list of Contacts from Firebase Project and show on Contact List screen.
- [ ] Delete option to delete contacts from contact list screen.



IMP NOTES:

- **Please take care in firebase user can access contacts only added by him/her not other users contact as well.**
- After completing the task create public repository in GitHub or Gitlab or Bitbucket and push the code.
- don't send zip file or attachment to email.
- If you don't know about git then you are not eligible.
- Code should be well formatted and follows Dart standard guidelines
- Handle all Errors and Exceptions with your own knowledge
- Create a release build & add in the release section


